# Call and Message Processor Application

This application processes JSON files containing call and message data. The processed data is stored and can be retrieved using several RESTful API endpoints. The application performs various operations, computes metrics, and KPIs based on the processed data. It's built with Java 11, Spring Boot, and uses an H2 database for data storage.

## Technologies Used

- Java
- Spring Boot
- JPA/Hibernate
- H2 Database - For testing and development purposes only. For production, please use a real SQL database.
- Jackson for JSON processing
- Log4j2 for logging

## Getting Started

Before running the application, make sure you have Java 11 installed on your system.

The application uses Spring Boot and Maven. To run the application, navigate to the root directory of the project where the `pom.xml` file is located and run the following command:

```bash
./mvnw spring-boot:run
```

Alternatively, you can use any IDE that supports Spring Boot and run the main application class.


## Features

- Processes JSON files that contain call and message data.
- Stores processed data for future retrieval and analysis.
- Corrects and processes data when recognizing JSON. However, the application doesn't tolerate incorrect or unrecognizable JSON format and will fail the processing operation, returning an HTTP 500 error.
- Provides endpoints to retrieve metrics and key performance indicators (KPIs) based on the processed data.

## API Endpoints

### 1. `/api/{date}`

This endpoint processes the JSON file that corresponds to the provided date. The date should be formatted as `yyyyMMdd`.

#### Request

`GET /api/{date}`

#### Path Parameters

- `date`: The date of the JSON file to be processed. Format: `yyyyMMdd`.

#### Response

- `200 OK`: The JSON file has been processed successfully.
- `400 Bad Request`: The provided date format is incorrect.
- `404 Not Found`: There is no JSON file for the provided date.
- `409 Conflict`: When the JSON file has already been processed.
- `500 Internal Server Error`: Any unexpected error that occurs during the processing, including unrecognizable or incorrect JSON format.

### 2. `/metrics`

This endpoint returns metrics about the last processed JSON files.

#### Request

`GET /metrics`

#### Response

- `200 OK`: The metrics have been retrieved successfully. The response body contains the metrics data.
- `204 No Content`: No metrics available. This could be due to no JSON files having been processed yet.
- `500 Internal Server Error`: Any unexpected error that occurs during the processing.

### 3. `/kpis`

This endpoint returns key performance indicators (KPIs) about the processed JSON files.

#### Request

`GET /kpis`

#### Response

- `200 OK`: The KPIs have been retrieved successfully. The response body contains the KPIs data.
- `204 No Content`: No KPIs available. This could be due to no JSON files having been processed yet.
- `500 Internal Server Error`: Any unexpected error that occurs during the processing.

## HTTP Status Codes

The following are HTTP status codes that might be returned by the API:

- `200 OK`: The request has been successfully processed.
- `204 No Content`: The server successfully processed the request, but is not returning any content. Typically for a GET request where there is no data to return.
- `400 Bad Request`: The request could not be understood by the server due to malformed syntax.
- `404 Not Found`: The requested resource could not be found.
- `409 Conflict`: When the JSON file has already been processed.
- `500 Internal Server Error`: An unexpected error occurred on the server side.
