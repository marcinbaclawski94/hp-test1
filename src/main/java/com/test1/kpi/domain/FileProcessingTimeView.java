package com.test1.kpi.domain;

import lombok.AllArgsConstructor;
import lombok.Value;

import java.time.Duration;

@Value
@AllArgsConstructor
public class FileProcessingTimeView {
  String fileName;
  Duration duration;
}
