package com.test1.kpi.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
@AllArgsConstructor
public class KpisView {
   Long totalNumberOfProcessedJsonFiles;
   Long totalNumberOfRows;
   Long totalNumberOfCalls;
   Long totalNumberOfMessages;
   Long totalNumberOfDifferentOriginCountryCodes;
   Long totalNumberOfDifferentDestinationCountryCodes;
   List<FileProcessingTimeView> fileProcessingTimes;}
