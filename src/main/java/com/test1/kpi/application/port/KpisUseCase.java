package com.test1.kpi.application.port;

import com.test1.kpi.domain.KpisView;
import org.springframework.http.ResponseEntity;

public interface KpisUseCase {
  ResponseEntity<KpisView> getKpis();
}
