package com.test1.kpi.application;

import com.test1.kpi.application.port.KpisUseCase;
import com.test1.kpi.domain.FileProcessingTimeView;
import com.test1.kpi.domain.KpisView;
import com.test1.processing.files.application.port.ProcessedFileUseCasesQuery;
import com.test1.processing.rows.application.port.ProcessedRowUseCaseQuery;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import lombok.val;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Log4j2
class KpisService implements KpisUseCase {
  private final ProcessedRowUseCaseQuery processedRowQuery;
  private final ProcessedFileUseCasesQuery processedFileServiceQuery;

  @Override
  public ResponseEntity<KpisView> getKpis() {
    log.info("Create KPIs starting ");
    final long countFiles = processedFileServiceQuery.countFiles();
    if(countFiles == 0){
      log.info("Creating KPIs finished no content found");
      return ResponseEntity.noContent().build();
    }
    final long countRows = processedRowQuery.countRows();
    final long countCalls = processedRowQuery.countCALL();
    final long countMsg = processedRowQuery.countMSG();
    final long countDistinctDestinationCountryCodes = processedRowQuery.countDistinctDestinationCountryCodes();
    final long countDistinctOriginCountryCodes = processedRowQuery.countDistinctOriginCountryCodes();
    val files = processedFileServiceQuery.findAllFiles().stream()
            .map(file -> new FileProcessingTimeView(file.getFileName(), file.getProcessingTime()))
            .collect(Collectors.toList());

    KpisView kpisView = KpisView.builder()
            .totalNumberOfProcessedJsonFiles(countFiles)
            .totalNumberOfCalls(countCalls)
            .totalNumberOfRows(countRows)
            .totalNumberOfMessages(countMsg)
            .totalNumberOfDifferentDestinationCountryCodes(countDistinctDestinationCountryCodes)
            .totalNumberOfDifferentOriginCountryCodes(countDistinctOriginCountryCodes)
            .fileProcessingTimes(files).build();

    log.info("The creation of KPIs was completed successfully.");
    log.debug("Result KPIs= {}",kpisView);
    return ResponseEntity.ok().body(kpisView);
  }
}
