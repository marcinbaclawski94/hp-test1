package com.test1.kpi.web;


import com.test1.kpi.application.port.KpisUseCase;
import com.test1.kpi.domain.KpisView;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityResult;

@RestController()
@RequiredArgsConstructor
class KpisController {
  private final KpisUseCase useCase;

  @GetMapping("/kpis")
  private ResponseEntity<KpisView> getKpis() {
    return useCase.getKpis();
  }
}
