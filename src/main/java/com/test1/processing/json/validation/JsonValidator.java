package com.test1.processing.json.validation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.test1.processing.files.domain.ProcessedFile;
import com.test1.processing.rows.domain.ProcessedRow;
import com.test1.processing.json.domain.JsonRecord;
import com.test1.processing.json.domain.MessageType;
import com.test1.processing.json.domain.StatusCode;
import com.test1.processing.json.domain.StatusMessage;
import lombok.extern.log4j.Log4j2;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Log4j2
public class JsonValidator {
  private final ObjectMapper objectMapper;
  private final PhoneNumberUtil phoneNumberUtil;


  public JsonValidator(ObjectMapper objectMapper) {
    this.phoneNumberUtil = PhoneNumberUtil.getInstance();
    this.objectMapper = objectMapper;
    this.objectMapper
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            .configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false)
            .configure(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE, false);

  }

  public List<ProcessedRow> processJsonString(ProcessedFile processedFile, List<String> jsonList) throws JsonProcessingException {
    log.info("Start processing Json List {}", jsonList.size());
    final List<ProcessedRow> processedRows = new ArrayList<>();
    for (String json : jsonList) {
      processedRows.add(processJsonString(processedFile, json));
    }
    log.info("Ends processing Json.Number of processed rows {}", processedRows.size());
    return processedRows;
  }

  private ProcessedRow processJsonString(ProcessedFile processedFile, String json) throws JsonProcessingException {
    log.debug("start processing Json {}", json);
    if (json.length() < 3) {
      log.debug("Json String empty");
      return ProcessedRow.emptyProcessedRow(processedFile);
    }
    val jsonRecord = objectMapper.readValue(json, JsonRecord.class);
    val processedRow = convertToProcessedRow(jsonRecord);
    processedRow.setProcessedFile(processedFile);
    log.debug("End processing Json. Json processed successfully {}", processedRow);
    return processedRow;
  }

  private ProcessedRow convertToProcessedRow(JsonRecord jsonRecord) {
    final String messageType = jsonRecord.getMessageType();

    if (messageType.equals(MessageType.MSG.name()) ||
            (!MessageType.isValid(messageType) && jsonRecord.getMessageContent() != null)) {
      return convertMessage(jsonRecord);
    } else if (messageType.equals(MessageType.CALL.name()) ||
            (!MessageType.isValid(messageType) && StringUtils.isNotEmpty(jsonRecord.getDuration()))) {
      return convertCall(jsonRecord);
    } else {
      log.warn("Not processed json {}", jsonRecord);
      return ProcessedRow.emptyProcessedRow(null);
    }

  }

  private ProcessedRow convertCall(JsonRecord jsonRecord) {
    log.debug("convertCall starting ");
    final boolean hasMissingFields = isCallHasMissingFields(jsonRecord);
    return ProcessedRow.builder()
            .type(MessageType.CALL)
            .originCode(extractCountryCode(jsonRecord.getOrigin()))
            .destinationCode(extractCountryCode(jsonRecord.getDestination()))
            .statusCode(extractStatusCode(jsonRecord.getStatusCode()))
            .hasMissingFields(hasMissingFields)
            .hasErrors(hasMissingFields || isCallHasErrorFields(jsonRecord))
            .hasBlankContent(false) // only MSG
            .duration(getInteger(jsonRecord.getDuration()))
            .build();

  }


  private ProcessedRow convertMessage(JsonRecord jsonRecord) {
    log.debug("convertMessage starting ");
    final boolean hasMissingFields = isMsgHasMissingFields(jsonRecord);
    return ProcessedRow.builder()
            .type(MessageType.MSG)
            .originCode(extractCountryCode(jsonRecord.getOrigin()))
            .destinationCode(extractCountryCode(jsonRecord.getDestination()))
            .hasMissingFields(hasMissingFields)
            .hasErrors(hasMissingFields || isMsgHasErrorFields(jsonRecord))
            .hasBlankContent(isMsgHasBlankContent(jsonRecord.getMessageContent()))
            .messageContent(jsonRecord.getMessageContent())
            .duration(null) // only CALL
            .statusCode(null) // only CALL
            .build();
  }


  private boolean isCallHasErrorFields(JsonRecord jsonRecord) {
    log.debug("isCallHasErrorFields starting");
    if (!jsonRecord.getMessageType().equals(MessageType.CALL.name())) {
      log.debug("Error in MessageType, messageType={}", jsonRecord.getMessageType());
      return true;
    }
    if (!NumberUtils.isParsable(jsonRecord.getTimestamp())) {
      log.debug("Error in Timestamp cannot be parse to number, timestamp={}", jsonRecord.getTimestamp());
      return true;
    }
    if (extractCountryCode(jsonRecord.getOrigin()).isEmpty()) {
      log.debug("Error in Origin cannot extract country,origin={}", jsonRecord.getOrigin());
      return true;
    }
    if (extractCountryCode(jsonRecord.getDestination()).isEmpty()) {
      log.debug("Error in Destination cannot extract country,destination={}", jsonRecord.getDestination());
      return true;
    }
    if (!NumberUtils.isParsable(jsonRecord.getDuration())) {
      log.debug("Error in Duration cannot be parse to number, timestamp={}", jsonRecord.getDuration());
      return true;
    }
    if (!StatusCode.isValid(jsonRecord.getStatusCode())) {
      log.debug("Error in Status Code is empty or is not valid,statusCode={}", jsonRecord.getStatusCode());
      return true;
    }
    if (jsonRecord.getStatusDescription().isEmpty()) {
      log.debug("Status Description empty");
      return true;
    }
    log.debug("isCallHasErrorFields ends no errors found");
    return false;
  }


  private boolean isMsgHasErrorFields(JsonRecord jsonRecord) {
    log.debug("isMsgHasErrorFields starting");
    if (!jsonRecord.getMessageType().equals(MessageType.MSG.name())) {
      log.debug("Error in MessageType, messageType= {}", jsonRecord.getMessageType());
      return true;
    }
    if (!NumberUtils.isParsable(jsonRecord.getTimestamp())) {
      log.debug("Error in Timestamp cannot be parse to number, timestamp= {}", jsonRecord.getTimestamp());
      return true;
    }
    if (extractCountryCode(jsonRecord.getOrigin()).isEmpty()) {
      log.debug("Error in Origin cannot extract country,origin= {}", jsonRecord.getOrigin());
      return true;
    }
    if (extractCountryCode(jsonRecord.getDestination()).isEmpty()) {
      log.debug("Error in Destination cannot extract country,destination= {}", jsonRecord.getDestination());
      return true;
    }
    if (jsonRecord.getMessageContent().isEmpty()) {
      log.debug("Error in Message Content is empty");
      return true;
    }
    if (jsonRecord.getMessageStatus().isEmpty() || !StatusMessage.isValid(jsonRecord.getMessageStatus())) {
      log.debug("Error in Message status is empty or is not valid,messageStatus= {}", jsonRecord.getMessageStatus());
      return true;
    }
    log.debug("isMsgHasErrorFields ends no errors found");
    return false;
  }


  private boolean isCallHasMissingFields(JsonRecord jsonRecord) {
    log.debug("isCallHasMissingFields starting");
    if (StringUtils.isEmpty(jsonRecord.getMessageType())) {
      log.debug("MessageType missing field");
      return true;
    }
    if (StringUtils.isEmpty(jsonRecord.getTimestamp())) {
      log.debug("Timestamp missing field");
      return true;
    }
    if (jsonRecord.getOrigin().isEmpty()) {
      log.debug("Origin missing field");
      return true;
    }
    if (jsonRecord.getDestination().isEmpty()) {
      log.debug("Destination missing field");
      return true;
    }
    if (StringUtils.isEmpty(jsonRecord.getDuration())) {
      log.debug("Duration missing field");
      return true;
    }
    if (StringUtils.isEmpty(jsonRecord.getStatusCode())) {
      log.debug("StatusCode missing field");
      return true;
    }
    if (StringUtils.isEmpty(jsonRecord.getStatusDescription())) {
      log.debug("StatusDescription missing field");
      return true;
    }
    log.debug("isCallHasMissingFields No missing fields");
    return false;
  }

  private boolean isMsgHasMissingFields(JsonRecord jsonRecord) {
    log.debug("isMsgHasMissingFields starting");
    if (StringUtils.isEmpty(jsonRecord.getMessageType())) {
      log.debug("MessageType missing field");
      return true;
    }
    if (StringUtils.isEmpty(jsonRecord.getTimestamp())) {
      log.debug("Timestamp missing field");
      return true;
    }
    if (StringUtils.isEmpty(jsonRecord.getOrigin())) {
      log.debug("Origin missing field");
      return true;
    }
    if (StringUtils.isEmpty(jsonRecord.getDestination())) {
      log.debug("Destination missing field");
      return true;
    }
    if (StringUtils.isEmpty(jsonRecord.getMessageContent())) {
      log.debug("Message Content missing field");
      return true;
    }
    if (StringUtils.isEmpty(jsonRecord.getMessageStatus())) {
      log.debug("Message Status missing field");
      return true;
    }
    log.debug("isMsgHasMissingFields ends no errors found");
    return false;
  }


  private StatusCode extractStatusCode(String statusCode) {
    if (StatusCode.isValid(statusCode)) {
      return StatusCode.valueOf(statusCode);
    } else {
      return null;
    }
  }

  private Integer getInteger(String value) {
    if (NumberUtils.isParsable(value)) {
      return Integer.valueOf(value);
    }
    return null;
  }

  private String extractCountryCode(String msisdn) {
    log.debug("extractCountryCode from MSISDN= {}", msisdn);
    if (StringUtils.isEmpty(msisdn) || msisdn.isBlank()) {
      log.debug("MSISDN is empty");
      return "";
    }
    try {
      if (!msisdn.startsWith("+")) {
        String phoneNumber = String.format("+%s", msisdn);
        return String.valueOf(phoneNumberUtil.parse(phoneNumber, "").getCountryCode());
      }
      return String.valueOf(phoneNumberUtil.parse(msisdn, "").getCountryCode());
    } catch (NumberParseException e) {
      log.warn("Cannot parse MSISDN= {}, {}", msisdn, e);
      return "";
    }
  }

  private boolean isMsgHasBlankContent(String messageContent) {
    final boolean result = messageContent == null || messageContent.isBlank();
    log.debug("isMsgHasBlankContent result= {} ", result);
    return result;
  }


}
