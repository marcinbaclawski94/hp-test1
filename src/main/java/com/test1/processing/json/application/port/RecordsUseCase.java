package com.test1.processing.json.application.port;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.FileNotFoundException;

public interface RecordsUseCase {
  void process(String date) throws FileNotFoundException, JsonProcessingException;
}
