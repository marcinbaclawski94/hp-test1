package com.test1.processing.json.application;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.test1.processing.files.application.port.ProcessedFileUseCasesCommand;
import com.test1.processing.files.domain.ProcessedFile;
import com.test1.processing.json.application.port.RecordsUseCase;
import com.test1.processing.json.domain.MessageType;
import com.test1.processing.json.file.FileReader;
import com.test1.processing.json.validation.JsonValidator;
import com.test1.processing.rows.application.port.ProcessedRowUseCaseCommand;
import com.test1.processing.rows.domain.ProcessedRow;
import com.test1.processing.words.application.port.WordUseCasesCommand;
import com.test1.processing.words.domain.Word;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.FileNotFoundException;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Log4j2
@RequiredArgsConstructor
class RecordService implements RecordsUseCase {
  private final FileReader fileReader;
  private final JsonValidator jsonValidator;
  private final ProcessedRowUseCaseCommand processedRowServiceCommand;
  private final ProcessedFileUseCasesCommand processedFileServiceCommand;
  private final WordUseCasesCommand wordUseCasesCommand;
  private final Map<String, Integer> wordOccurrences = new HashMap<>();
  @Value("${target.words}")
  private String[] targetWords;


  @Override
  @Transactional(rollbackOn = {JacksonException.class})
  public void process(String date) throws FileNotFoundException, JsonProcessingException {
    log.info("Processing Json start date= {}", date);
    final List<String> jsonDataList = fileReader.readDataFromFile(date);
    final ProcessedFile processedFile = processedFileServiceCommand.saveAndFlushProcessedFile(new ProcessedFile(date, Duration.ZERO));
    Duration processingTime = processRows(jsonDataList, processedFile);
    processedFileServiceCommand.updateProcessingTimeInProcessedFile(processedFile, processingTime);
    log.info("Processing Json ends successful");
  }

  private Duration processRows(List<String> jsonDataList, ProcessedFile processedFile) throws JsonProcessingException {
    Instant startTime = Instant.now();
    final List<ProcessedRow> processedRows = jsonValidator.processJsonString(processedFile, jsonDataList);
    processedRowServiceCommand.saveAllProcessedRows(processedRows);
    saveWords(processedRows, processedFile);
    return Duration.between(startTime, Instant.now());
  }

  private void saveWords(List<ProcessedRow> processedRows, ProcessedFile processedFile) {
    calculateTargetWordsFromContent(processedRows);
    List<Word> wordOccurrencesList = wordOccurrences.entrySet().stream()
            .map(entry -> new Word(entry.getKey(), entry.getValue(), processedFile))
            .collect(Collectors.toList());
    wordUseCasesCommand.saveAllWords(wordOccurrencesList);
  }

  private void calculateTargetWordsFromContent(List<ProcessedRow> processedRows) {
    final List<String> targetWordsList = getAllowedTargetWordsList();
    for (ProcessedRow processedRow : processedRows) {
      if (processedRow.getType() == MessageType.MSG) {
        calculateTargetWordsFromContent(processedRow.getMessageContent(), targetWordsList);
      }
    }
  }

  private void calculateTargetWordsFromContent(String messageContent, List<String> targetWordsList) {
    if (messageContent == null || messageContent.isBlank()) {
      return;
    }

    String[] words = messageContent.split("\\s+");

    for (String word : words) {
      String upperCaseWord = word.toUpperCase();
      if (targetWordsList.contains(upperCaseWord)) {
        wordOccurrences.put(upperCaseWord, wordOccurrences.getOrDefault(upperCaseWord, 0) + 1);
      }
    }
  }

  private List<String> getAllowedTargetWordsList() {
    return Arrays.stream(targetWords).collect(Collectors.toList());

  }


}
