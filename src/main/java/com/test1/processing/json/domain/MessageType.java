package com.test1.processing.json.domain;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;


@RequiredArgsConstructor
public enum MessageType {
  CALL, MSG;

  public static boolean isValid(String type) {
    if (StringUtils.isEmpty(type)) {
      return false;
    }
    try {
      StatusCode.valueOf(type);
      return true;
    } catch (IllegalArgumentException e) {
      return false;
    }
  }
}
