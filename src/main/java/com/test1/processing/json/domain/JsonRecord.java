package com.test1.processing.json.domain;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Value;

@Value
public class JsonRecord {
  @JsonAlias({"message_type", "messageType"})
  String messageType;
  String timestamp;
  String origin;
  String destination;
  String duration; //CALL ONLY
  @JsonAlias({"status_code", "statusCode"})
  String statusCode;
  @JsonAlias({"status_description", "statusDescription"})
  String statusDescription;
  @JsonAlias({"message_content", "messageContent"})
  String messageContent;
  @JsonAlias({"message_status", "messageStatus"})
  String messageStatus;

}
