package com.test1.processing.json.domain;


import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;

@RequiredArgsConstructor
public enum StatusCode {
  OK, KO;

  public static boolean isValid(String statusCode) {
    if (StringUtils.isEmpty(statusCode)) {
      return false;
    }
    try {
      StatusCode.valueOf(statusCode);
      return true;
    } catch (IllegalArgumentException e) {
      return false;
    }
  }
}