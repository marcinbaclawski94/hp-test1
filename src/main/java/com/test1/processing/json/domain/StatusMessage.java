package com.test1.processing.json.domain;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;

@RequiredArgsConstructor
public enum StatusMessage {
  DELIVERED, SEEN;

  public static boolean isValid(String value) {
    if (StringUtils.isEmpty(value)) {
      return false;
    }
    try {
      StatusMessage.valueOf(value);
      return true;
    } catch (IllegalArgumentException e) {
      return false;
    }
  }

}
