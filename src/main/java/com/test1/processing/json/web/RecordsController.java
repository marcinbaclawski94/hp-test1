package com.test1.processing.json.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.test1.processing.json.application.port.RecordsUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

@RestController
@RequiredArgsConstructor
class RecordsController {
  private final RecordsUseCase useCase;

  @GetMapping("/api/{date}")
  private ResponseEntity<?> process(@PathVariable String date) throws FileNotFoundException, JsonProcessingException {
    try {
      LocalDate dateParse = LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyyMMdd"));
      if (dateParse.isAfter(LocalDate.now())) {
        return new ResponseEntity<>("Date is bigger than today.", HttpStatus.BAD_REQUEST);
      }
    } catch (DateTimeParseException e) {
      return new ResponseEntity<>("Wrong date format.Please correct", HttpStatus.BAD_REQUEST);
    }
    useCase.process(date);
    return ResponseEntity.ok().build();
  }
}
