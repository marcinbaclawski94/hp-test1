package com.test1.processing.json.file;

import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Log4j2
@NoArgsConstructor
@Component
public class FileReader {
  private static final String FILE_PREFIX="MCP_";
  private static final String FILE_EXTENSION="json";

  @Value("${files.location:src/main/resources/logs}")
  private String filesLocation;


  public List<String> readDataFromFile(String date) throws FileNotFoundException {
    String fileLocation = String.format("%s/%s%s.%s",filesLocation,FILE_PREFIX,date,FILE_EXTENSION);
    log.debug("Read json data from {}",fileLocation);
    Scanner scanner = new Scanner(new File(fileLocation));
    List<String> jsonData = new ArrayList<>();
    while (scanner.hasNextLine()) {
      String line = scanner.nextLine();
      log.debug("Json data {}",line);
      jsonData.add(line);
    }
    scanner.close();
    log.debug("Number of json records found in file {}",jsonData.size());
    return jsonData;
  }
}
