package com.test1.processing.words.application;

import com.test1.processing.words.application.port.WordUseCasesCommand;
import com.test1.processing.words.application.port.WordUseCasesQuery;
import com.test1.processing.words.db.WordRepository;
import com.test1.processing.words.domain.Word;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@AllArgsConstructor
class WordService implements WordUseCasesCommand, WordUseCasesQuery {
  private final WordRepository repository;


  @Override
  public void saveAllWords(List<Word> words) {
    repository.saveAllAndFlush(words);
  }

  @Override
  public List<Word> findAllWordsFromLatestFile() {
    return repository.findAllWordsFromLatestFile();
  }
}
