package com.test1.processing.words.application.port;

import com.test1.processing.words.domain.Word;

import java.util.List;

public interface WordUseCasesQuery {

    List<Word> findAllWordsFromLatestFile();
}
