package com.test1.processing.words.domain;

import com.test1.processing.files.domain.ProcessedFile;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Word {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  @Column(nullable = false)
  private String word;
  @Column(nullable = false)
  @Builder.Default
  private Integer count = 0;
  @ManyToOne
  @JoinColumn(name = "processed_file_id")
  private ProcessedFile processedFile;

  public Word(String word, Integer count, ProcessedFile processedFile) {
    this.word = word;
    this.count = count;
    this.processedFile = processedFile;
  }
}