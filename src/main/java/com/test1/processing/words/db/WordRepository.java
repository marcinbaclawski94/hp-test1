package com.test1.processing.words.db;

import com.test1.processing.words.domain.Word;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface WordRepository extends JpaRepository<Word, Integer> {

  @Query("SELECT word FROM Word word JOIN word.processedFile file WHERE file.timestamp =" +
          " (SELECT MAX(file.timestamp) FROM ProcessedFile file) ORDER BY word.count DESC")
  List<Word> findAllWordsFromLatestFile();
}
