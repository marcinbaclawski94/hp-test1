package com.test1.processing.rows.domain;


import com.test1.processing.files.domain.ProcessedFile;
import com.test1.processing.json.domain.MessageType;
import com.test1.processing.json.domain.StatusCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "ProcessedRow")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProcessedRow {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @ManyToOne(optional = false)
  @JoinColumn(name = "fileId", nullable = false)
  private ProcessedFile processedFile;

  @Enumerated(EnumType.STRING)
  private MessageType type;

  @Column(name = "originCode")
  private String originCode;

  @Column(name = "destinationCode")
  private String destinationCode;

  private Integer duration;

  @Enumerated(EnumType.STRING)
  @Column(name = "statusCode")
  private StatusCode statusCode; //CALL

  @Column(name = "hasErrors", nullable = false)
  private Boolean hasErrors;

  @Column(name = "hasMissingFields", nullable = false)
  private Boolean hasMissingFields;

  @Column(name = "hasBlankContent", nullable = false)
  private Boolean hasBlankContent; //MSG ONLY

  private transient String messageContent;



  public static ProcessedRow emptyProcessedRow(ProcessedFile processedFile) {
    return ProcessedRow.builder()
            .processedFile(processedFile)
            .hasErrors(true)
            .hasBlankContent(true)
            .hasMissingFields(true)
            .build();
  }
}
