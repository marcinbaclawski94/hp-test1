package com.test1.processing.rows.db;

import com.test1.processing.rows.domain.ProcessedRow;
import com.test1.processing.json.domain.MessageType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProcessedRowRepository extends JpaRepository<ProcessedRow, Integer> {


  @Query("SELECT count(distinct destinationCode) FROM ProcessedRow")
  long countDistinctDestinationCountryCodes();

  @Query("SELECT count(distinct originCode) FROM ProcessedRow")
  long countDistinctOriginCountryCodes();

  long countByType(MessageType type);

  @Query("SELECT row FROM ProcessedRow row JOIN row.processedFile file WHERE file.timestamp =" +
          " (SELECT MAX(file.timestamp) FROM ProcessedFile file)")
  List<ProcessedRow> findAllRowsFromLatestFile();

  @Query("SELECT COUNT(row) FROM ProcessedRow row JOIN row.processedFile file WHERE file.timestamp =" +
          " (SELECT MAX(file.timestamp) FROM ProcessedFile file) AND row.hasErrors = true")
  long countRowsWithFieldErrorsFromLatestFile();

  @Query("SELECT COUNT(row) FROM ProcessedRow row JOIN row.processedFile file WHERE file.timestamp = " +
          "(SELECT MAX(file.timestamp) FROM ProcessedFile file) AND row.hasMissingFields = true")
  long countRowsWithMissingFieldsFromLatestFile();

  @Query("SELECT COUNT(row) FROM ProcessedRow row JOIN row.processedFile file WHERE row.type = 'MSG' AND row.hasBlankContent = true AND file.timestamp =" +
          " (SELECT MAX(file.timestamp) FROM ProcessedFile file)")
  long countMessagesWithBlankContentFromLatestFile();
  }
