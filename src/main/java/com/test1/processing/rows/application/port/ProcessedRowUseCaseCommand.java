package com.test1.processing.rows.application.port;

import com.test1.processing.rows.domain.ProcessedRow;

import java.util.List;

public interface ProcessedRowUseCaseCommand {
   void saveAllProcessedRows(List<ProcessedRow> rows);
}
