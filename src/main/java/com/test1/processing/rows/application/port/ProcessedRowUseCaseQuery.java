package com.test1.processing.rows.application.port;

import com.test1.processing.rows.domain.ProcessedRow;

import java.util.List;

public interface ProcessedRowUseCaseQuery {
  List<ProcessedRow> findAllRowsFromLatestFile();

  long countRows();

  long countMSG();

  long countCALL();

  long countRowsWithMissingFieldsFromLatestFile();

  long countMessagesWithBlankContentFromLatestFile();

  long countRowsWithFieldErrorsFromLatestFile();

  long countDistinctDestinationCountryCodes();

  long countDistinctOriginCountryCodes();
}
