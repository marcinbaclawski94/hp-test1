package com.test1.processing.rows.application;

import com.test1.processing.rows.application.port.ProcessedRowUseCaseCommand;
import com.test1.processing.rows.application.port.ProcessedRowUseCaseQuery;
import com.test1.processing.rows.db.ProcessedRowRepository;
import com.test1.processing.rows.domain.ProcessedRow;
import com.test1.processing.json.domain.MessageType;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@AllArgsConstructor
class ProcessedRowService implements ProcessedRowUseCaseQuery, ProcessedRowUseCaseCommand {
    private final ProcessedRowRepository repository;

    public void saveAllProcessedRows(List<ProcessedRow> rows) {
        repository.saveAll(rows);
    }

    public List<ProcessedRow> findAllRowsFromLatestFile() {
        return repository.findAllRowsFromLatestFile();
    }

    public long countRows() {
        return repository.count();
    }

    public long countMSG() {
        return repository.countByType(MessageType.MSG);
    }

    public long countCALL() {
        return repository.countByType(MessageType.CALL);
    }

    public long countRowsWithMissingFieldsFromLatestFile() {
        return repository.countRowsWithMissingFieldsFromLatestFile();
    }

    public long countMessagesWithBlankContentFromLatestFile() {
        return repository.countMessagesWithBlankContentFromLatestFile();
    }

    public long countRowsWithFieldErrorsFromLatestFile() {
        return repository.countRowsWithFieldErrorsFromLatestFile();
    }

    public long countDistinctDestinationCountryCodes() {
        return repository.countDistinctDestinationCountryCodes();
    }

    public long countDistinctOriginCountryCodes() {
        return repository.countDistinctOriginCountryCodes();
    }


}
