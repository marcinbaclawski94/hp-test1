package com.test1.processing.files.application.port;

import com.test1.processing.files.domain.ProcessedFile;

import java.util.List;

public interface ProcessedFileUseCasesQuery {
     long countFiles();

     List<ProcessedFile> findAllFiles();
}
