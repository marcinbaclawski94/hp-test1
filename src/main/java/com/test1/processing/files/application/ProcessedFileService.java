package com.test1.processing.files.application;

import com.test1.processing.files.application.port.ProcessedFileUseCasesCommand;
import com.test1.processing.files.application.port.ProcessedFileUseCasesQuery;
import com.test1.processing.files.db.ProcessedFileRepository;
import com.test1.processing.files.domain.ProcessedFile;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.List;


@Service
@AllArgsConstructor
class ProcessedFileService implements ProcessedFileUseCasesCommand, ProcessedFileUseCasesQuery {
  private final ProcessedFileRepository repository;


   public ProcessedFile saveAndFlushProcessedFile(ProcessedFile file) {
    return repository.saveAndFlush(file);
  }

   public void updateProcessingTimeInProcessedFile(ProcessedFile file, Duration processingTime) {
    file.setProcessingTime(processingTime);
    repository.saveAndFlush(file);
  }

  public long countFiles() {
    return repository.count();
  }

  public List<ProcessedFile> findAllFiles() {
    return repository.findAll();
  }

}
