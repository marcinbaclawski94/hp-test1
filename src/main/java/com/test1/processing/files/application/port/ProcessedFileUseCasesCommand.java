package com.test1.processing.files.application.port;

import com.test1.processing.files.domain.ProcessedFile;

import java.time.Duration;

public interface ProcessedFileUseCasesCommand {
     ProcessedFile saveAndFlushProcessedFile(ProcessedFile file);
     void updateProcessingTimeInProcessedFile(ProcessedFile file, Duration processingTime);


}
