package com.test1.processing.files.db;

import com.test1.processing.files.domain.ProcessedFile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProcessedFileRepository extends JpaRepository<ProcessedFile, Integer> {


}
