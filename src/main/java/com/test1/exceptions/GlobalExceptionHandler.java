package com.test1.exceptions;


import com.fasterxml.jackson.core.JsonParseException;
import org.h2.jdbc.JdbcSQLIntegrityConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
class GlobalExceptionHandler {

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<Object> handleValidationException(MethodArgumentNotValidException ex) {
    List<String> errors = ex.getBindingResult()
            .getFieldErrors()
            .stream()
            .map(FieldError::getDefaultMessage)
            .collect(Collectors.toList());

    return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(FileNotFoundException.class)
  public ResponseEntity<Object> handleValidationException(FileNotFoundException ex) {
    return ResponseEntity.notFound().build();
  }

  @ExceptionHandler(JdbcSQLIntegrityConstraintViolationException.class)
  public ResponseEntity<Object> handleValidationException(JdbcSQLIntegrityConstraintViolationException ex) {
    final int length = ex.getOriginalMessage().length();
    final String date = ex.getOriginalMessage().substring(length - 12, length - 5);
    return new ResponseEntity<>(String.format("File %s already processed", date), HttpStatus.CONFLICT);
  }

  @ExceptionHandler(JsonParseException.class)
  public ResponseEntity<String> handleJsonParseException(JsonParseException e) {
    return new ResponseEntity<>(String.format("Invalid JSON format: %s", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
  }

}
