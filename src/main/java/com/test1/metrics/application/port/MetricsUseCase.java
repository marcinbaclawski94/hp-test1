package com.test1.metrics.application.port;

import com.test1.metrics.domain.MetricsView;
import org.springframework.http.ResponseEntity;

public interface MetricsUseCase {
  ResponseEntity<MetricsView> getMetrics();
}
