package com.test1.metrics.application;

import com.test1.metrics.application.port.MetricsUseCase;
import com.test1.metrics.domain.AvgDurationCallView;
import com.test1.metrics.domain.MetricsView;
import com.test1.metrics.domain.OriginalDestinationCallView;
import com.test1.metrics.domain.StatusCallCounterView;
import com.test1.metrics.domain.WordOccurrenceView;
import com.test1.processing.rows.application.port.ProcessedRowUseCaseQuery;
import com.test1.processing.rows.domain.ProcessedRow;
import com.test1.processing.json.domain.MessageType;
import com.test1.processing.words.application.port.WordUseCasesQuery;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Log4j2
class MetricsService implements MetricsUseCase {
  private final ProcessedRowUseCaseQuery processedRowUseCaseQuery;
  private final WordUseCasesQuery wordUseCasesQuery;

  @Override
  public ResponseEntity<MetricsView> getMetrics() {
    log.info("Create Metrics starting ");
    final List<ProcessedRow> processedRows = processedRowUseCaseQuery.findAllRowsFromLatestFile();
    if(processedRows.isEmpty()){
      log.info("Creating Metrics finished no content found");
      return ResponseEntity.noContent().build();
    }

    log.debug("Create Metrics for file {}",processedRows.get(0).getProcessedFile().getId());
    final long numberOfRowsWithMissingFields = getCountOfRowsWithMissingFields(processedRows);
    final long numberOfMessagesWithBlankContent = getCountOfMessagesWithBlankContent(processedRows);
    final long numberOfRowsWithFieldErrors = getCountOfRowsWithFieldErrors(processedRows);
    final List<OriginalDestinationCallView> originDestinationGroupedByCountryCode = getOriginDestinationGroupedByCountryCode(processedRows);
    final List<AvgDurationCallView> averageCallDurationByCountry = getAverageCallDurationByCountry(processedRows);
    final List<StatusCallCounterView> callStatusRelationship = getCallStatusRelationship(processedRows);
    MetricsView metricsView = MetricsView.builder()
            .numberOfRowsWithMissingFields(numberOfRowsWithMissingFields)
            .numberOfRowsWithFieldErrors(numberOfRowsWithFieldErrors)
            .numberOfMessagesWithBlankContent(numberOfMessagesWithBlankContent)
            .relationshipBetweenOkKoCalls(callStatusRelationship)
            .averageCallDurationGroupedByCountryCode(averageCallDurationByCountry)
            .callsOriginDestinationGroupedByCountryCode(originDestinationGroupedByCountryCode)
            .wordsRanking(getWordRanking())
            .build();

    log.info("The creation of Metrics was completed successfully.");
    log.debug("Result Metrics= {}",metricsView);
    return ResponseEntity.ok(metricsView);
  }

  private List<WordOccurrenceView> getWordRanking() {
    return wordUseCasesQuery.findAllWordsFromLatestFile().stream().map(e -> new WordOccurrenceView(e.getWord(), e.getCount())).collect(Collectors.toList());
  }

  private List<OriginalDestinationCallView> getOriginDestinationGroupedByCountryCode(List<ProcessedRow> processedRows) {
    final Map<String, Long> originGroupedByCountryCode = getOriginGroupedByCountryCode(processedRows);
    final Map<String, Long> destinationGroupedByCountryCode = getDestinationGroupedByCountryCode(processedRows);
    return originGroupedByCountryCode.entrySet().stream()
            .map(e -> new OriginalDestinationCallView(e.getKey(), e.getValue(), destinationGroupedByCountryCode.getOrDefault(e.getKey(), 0L)))
            .collect(Collectors.toList());
  }

  Map<String, Long> getOriginGroupedByCountryCode(List<ProcessedRow> processedRows) {
    return processedRows.stream()
            .filter(ObjectUtils::isNotEmpty)
            .filter(row -> row.getType() == MessageType.CALL)
            .filter(row -> StringUtils.isNotEmpty(row.getOriginCode()))
            .collect(Collectors.groupingBy(ProcessedRow::getOriginCode, Collectors.counting()));
  }

  Map<String, Long> getDestinationGroupedByCountryCode(List<ProcessedRow> processedRows) {
    return processedRows.stream()
            .filter(ObjectUtils::isNotEmpty)
            .filter(row -> row.getType() == MessageType.CALL)
            .filter(row -> StringUtils.isNotEmpty(row.getDestinationCode()))
            .collect(Collectors.groupingBy(ProcessedRow::getDestinationCode, Collectors.counting()));
  }

  private long getCountOfRowsWithMissingFields(List<ProcessedRow> processedRows) {
    return processedRows.stream().filter(ProcessedRow::getHasMissingFields).count();
  }

  private long getCountOfMessagesWithBlankContent(List<ProcessedRow> processedRows) {
    return processedRows.stream()
            .filter(ObjectUtils::isNotEmpty)
            .filter(e -> e.getType() == MessageType.MSG)
            .filter(ProcessedRow::getHasBlankContent)
            .count();
  }

  private List<StatusCallCounterView> getCallStatusRelationship(List<ProcessedRow> processedRows) {
    return processedRows.stream()
            .filter(ObjectUtils::isNotEmpty)
            .filter(row -> row.getType() == MessageType.CALL)
            .filter(row -> row.getStatusCode() != null)
            .collect(Collectors.groupingBy(ProcessedRow::getStatusCode, Collectors.counting())).entrySet()
            .stream().map(e -> new StatusCallCounterView(e.getKey(), e.getValue())).collect(Collectors.toList());
  }


  private List<AvgDurationCallView> getAverageCallDurationByCountry(List<ProcessedRow> processedRows) {
    return processedRows.stream()
            .filter(ObjectUtils::isNotEmpty)
            .filter(row -> row.getType() == MessageType.CALL)
            .filter(row -> StringUtils.isNotEmpty(row.getOriginCode()))
            .filter(row -> row.getDuration() != null)
            .collect(Collectors.groupingBy(ProcessedRow::getOriginCode, Collectors.averagingInt(ProcessedRow::getDuration)))
            .entrySet().stream().map(e -> new AvgDurationCallView(e.getKey(), e.getValue())).collect(Collectors.toList());
  }


  private long getCountOfRowsWithFieldErrors(List<ProcessedRow> processedRows) {
    return processedRows.stream()
            .filter(ObjectUtils::isNotEmpty)
            .filter(ProcessedRow::getHasErrors).count();
  }
}
