package com.test1.metrics.web;


import com.test1.metrics.application.port.MetricsUseCase;
import com.test1.metrics.domain.MetricsView;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
class MetricsController {
  private final MetricsUseCase useCase;

  @GetMapping("/metrics")
  private ResponseEntity<MetricsView> getMetrics()  {
   return useCase.getMetrics();
  }
}
