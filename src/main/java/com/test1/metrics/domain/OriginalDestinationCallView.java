package com.test1.metrics.domain;

import lombok.Value;

@Value
public class OriginalDestinationCallView {
  String countryCode;
  Long originalCount;
  Long destinationCount;
}
