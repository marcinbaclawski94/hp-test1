package com.test1.metrics.domain;

import lombok.Value;

@Value
public class WordOccurrenceView {
  String words;
  Integer occurrence;
}
