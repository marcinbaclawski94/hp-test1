package com.test1.metrics.domain;

import lombok.Value;

@Value
public class AvgDurationCallView {
  String countryCode;
  Double avgDuration;
}
