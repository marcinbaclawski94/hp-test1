package com.test1.metrics.domain;

import com.test1.processing.json.domain.StatusCode;
import lombok.Value;

@Value
public class StatusCallCounterView {
  StatusCode statusCode;
  Long count;
}
