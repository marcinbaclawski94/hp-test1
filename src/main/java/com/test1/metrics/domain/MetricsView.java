package com.test1.metrics.domain;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class MetricsView {
    Long numberOfRowsWithMissingFields;
    Long numberOfMessagesWithBlankContent;
    Long numberOfRowsWithFieldErrors;
    List<OriginalDestinationCallView> callsOriginDestinationGroupedByCountryCode;
    List<StatusCallCounterView> relationshipBetweenOkKoCalls;
    List<AvgDurationCallView> averageCallDurationGroupedByCountryCode;
    List<WordOccurrenceView> wordsRanking;
}
