package com.test1.metrics.web;

import com.test1.metrics.domain.AvgDurationCallView;
import com.test1.metrics.domain.MetricsView;
import com.test1.metrics.domain.OriginalDestinationCallView;
import com.test1.metrics.domain.StatusCallCounterView;
import com.test1.metrics.domain.WordOccurrenceView;
import com.test1.processing.json.domain.StatusCode;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;

import javax.annotation.PostConstruct;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@AutoConfigureTestDatabase
class MetricsControllerTestIT {

  @LocalServerPort
  private int port;
  private String uri;

  @PostConstruct
  void init() {
    uri = "http://localhost:" + port;
  }

  @Test
  void ShouldReturnMetrics() {
    given()
            .pathParam("date", "20200101")
            .when()
            .get(uri + "/api/{date}")
            .then()
            .statusCode(HttpStatus.OK.value());

    final MetricsView result = given()
            .when()
            .get(uri + "/metrics")
            .then()
            .statusCode(HttpStatus.OK.value())
            .extract().as(MetricsView.class);


    assertThat(result.getNumberOfMessagesWithBlankContent()).isEqualTo(1);
    assertThat(result.getNumberOfRowsWithFieldErrors()).isEqualTo(3);
    assertThat(result.getNumberOfRowsWithMissingFields()).isEqualTo(2);
    final OriginalDestinationCallView originalDestinationCallView = result.getCallsOriginDestinationGroupedByCountryCode().get(0);

    assertThat(originalDestinationCallView.getCountryCode()).isEqualTo("34");
    assertThat(originalDestinationCallView.getDestinationCount()).isEqualTo(3);
    assertThat(originalDestinationCallView.getOriginalCount()).isEqualTo(3);
    final StatusCallCounterView statusCallCounterView = result.getRelationshipBetweenOkKoCalls().get(0);

    assertThat(statusCallCounterView.getStatusCode()).isEqualTo(StatusCode.OK);
    assertThat(statusCallCounterView.getCount()).isEqualTo(2);

    final AvgDurationCallView avgDurationCallView = result.getAverageCallDurationGroupedByCountryCode().get(0);

    assertThat(avgDurationCallView.getCountryCode()).isEqualTo("34");
    assertThat(avgDurationCallView.getAvgDuration()).isBetween(110D, 120D);

    final WordOccurrenceView wordOccurrenceView = result.getWordsRanking().get(0);

    assertThat(wordOccurrenceView.getOccurrence()).isEqualTo(2);
    assertThat(wordOccurrenceView.getWords()).isEqualTo("HELLO");

  }

  @Test
  void shouldReturnNoContent() {
    given()
            .when()
            .get(uri + "/metrics")
            .then()
            .statusCode(HttpStatus.NO_CONTENT.value());
  }
}