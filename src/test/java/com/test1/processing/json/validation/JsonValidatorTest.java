package com.test1.processing.json.validation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.test1.processing.files.domain.ProcessedFile;
import com.test1.processing.rows.domain.ProcessedRow;
import com.test1.processing.json.domain.MessageType;
import com.test1.processing.json.domain.StatusCode;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Duration;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class JsonValidatorTest {

  @Autowired
  private JsonValidator underTest;

  @Test
  void shouldProcessedCALLCorrected() throws JsonProcessingException {
    ProcessedFile file = new ProcessedFile("testFile", Duration.ZERO);
    String json = "{\"message_type\": \"CALL\",\"timestamp\": 1517645700,\"origin\": 34969000001,\"destination\": 34969000101," +
            "\"duration\": 120,\"status_code\": \"OK\",\"status_description\": \"OK\"}";

    var result = underTest.processJsonString(file, Collections.singletonList(json));

    assertThat(result).hasSize(1);

    final ProcessedRow processedRow = result.get(0);

    assertThat(processedRow.getType()).isEqualTo(MessageType.CALL);
    assertThat(processedRow.getOriginCode()).isEqualTo("34");
    assertThat(processedRow.getDestinationCode()).isEqualTo("34");
    assertThat(processedRow.getDuration()).isEqualTo(120);
    assertThat(processedRow.getStatusCode()).isEqualTo(StatusCode.OK);
    assertThat(processedRow.getHasMissingFields()).isFalse();
    assertThat(processedRow.getHasErrors()).isFalse();
    assertThat(processedRow.getHasBlankContent()).isFalse();
    assertThat(processedRow.getMessageContent()).isNull();
  }


  @Test
  void shouldProcessedMSGCorrected() throws JsonProcessingException {
    ProcessedFile file = new ProcessedFile("testFile", Duration.ZERO);
    String json = "{\"message_type\": \"MSG\",\"timestamp\": 1517559300,\"origin\": 34960000001," +
            "\"destination\": 34960000101,\"message_content\": \"1. HELLO\",\"message_status\": \"DELIVERED\"}";

    var result = underTest.processJsonString(file, Collections.singletonList(json));

    assertThat(result).hasSize(1);

    final ProcessedRow processedRow = result.get(0);

    assertThat(processedRow.getType()).isEqualTo(MessageType.MSG);
    assertThat(processedRow.getOriginCode()).isEqualTo("34");
    assertThat(processedRow.getDestinationCode()).isEqualTo("34");
    assertThat(processedRow.getMessageContent()).isEqualTo("1. HELLO");
    assertThat(processedRow.getHasMissingFields()).isFalse();
    assertThat(processedRow.getHasErrors()).isFalse();
    assertThat(processedRow.getHasBlankContent()).isFalse();
  }

  @Test
  void shouldProcessedMSGWithMissingContent() throws JsonProcessingException {
    ProcessedFile file = new ProcessedFile("testFile", Duration.ZERO);
    String json = "{\"message_type\": \"MSG\",\"timestamp\": 1517559300,\"origin\": 34960000001," +
            "\"destination\": 34960000101,\"message_content\": \"\",\"message_status\": \"DELIVERED\"}";

    var result = underTest.processJsonString(file, Collections.singletonList(json));

    assertThat(result).hasSize(1);

    final ProcessedRow processedRow = result.get(0);

    assertThat(processedRow.getType()).isEqualTo(MessageType.MSG);
    assertThat(processedRow.getOriginCode()).isEqualTo("34");
    assertThat(processedRow.getDestinationCode()).isEqualTo("34");
    assertThat(processedRow.getMessageContent()).isEmpty();
    assertThat(processedRow.getHasMissingFields()).isTrue();
    assertThat(processedRow.getHasErrors()).isTrue();
    assertThat(processedRow.getHasBlankContent()).isTrue();
  }

  @Test
  void shouldRecognizedAndProcessedAsMSGType() throws JsonProcessingException {
    ProcessedFile file = new ProcessedFile("testFile", Duration.ZERO);
    String json = "{\"message_type\": \"\",\"timestamp\": 1517559300,\"origin\": 34960000001," +
            "\"destination\": 34960000101,\"message_content\": \"1. HELLO\",\"message_status\": \"DELIVERED\"}";

    var result = underTest.processJsonString(file, Collections.singletonList(json));

    assertThat(result).hasSize(1);

    final ProcessedRow processedRow = result.get(0);

    assertThat(processedRow.getType()).isEqualTo(MessageType.MSG);
    assertThat(processedRow.getOriginCode()).isEqualTo("34");
    assertThat(processedRow.getDestinationCode()).isEqualTo("34");
    assertThat(processedRow.getMessageContent()).isEqualTo("1. HELLO");
    assertThat(processedRow.getHasMissingFields()).isTrue();
    assertThat(processedRow.getHasErrors()).isTrue();
    assertThat(processedRow.getHasBlankContent()).isFalse();
  }


  @Test
  void shouldRecognizedAndProcessedAsCALLType() throws JsonProcessingException {
    ProcessedFile file = new ProcessedFile("testFile", Duration.ZERO);
    String json = "{\"message_type\": \"\",\"timestamp\": null, \"origin\": 34969000001,\"destination\": 34969000101," +
            "\"duration\": 120,\"status_code\": \"OK\",\"status_description\": \"OK\"}";

    var result = underTest.processJsonString(file, Collections.singletonList(json));

    assertThat(result).hasSize(1);

    final ProcessedRow processedRow = result.get(0);

    assertThat(processedRow.getType()).isEqualTo(MessageType.CALL);
    assertThat(processedRow.getOriginCode()).isEqualTo("34");
    assertThat(processedRow.getDestinationCode()).isEqualTo("34");
    assertThat(processedRow.getDuration()).isEqualTo(120);
    assertThat(processedRow.getStatusCode()).isEqualTo(StatusCode.OK);
    assertThat(processedRow.getHasMissingFields()).isTrue();
    assertThat(processedRow.getHasErrors()).isTrue();
    assertThat(processedRow.getHasBlankContent()).isFalse();
    assertThat(processedRow.getMessageContent()).isNull();
  }

  @Test
  void shouldProcessedAsCALLWhenFieldsEmpty() throws JsonProcessingException {
    ProcessedFile file = new ProcessedFile("testFile", Duration.ZERO);
    String json = "{\"message_type\":\"CALL\",\"timestamp\": null, \"origin\": null,\"destination\": null," +
            "\"duration\": null,\"status_code\": null,\"status_description\": null}";

    var result = underTest.processJsonString(file, Collections.singletonList(json));

    assertThat(result).hasSize(1);

    final ProcessedRow processedRow = result.get(0);

    assertThat(processedRow.getType()).isEqualTo(MessageType.CALL);
    assertThat(processedRow.getOriginCode()).isEmpty();
    assertThat(processedRow.getDestinationCode()).isEmpty();
    assertThat(processedRow.getDuration()).isNull();
    assertThat(processedRow.getStatusCode()).isNull();
    assertThat(processedRow.getHasMissingFields()).isTrue();
    assertThat(processedRow.getHasErrors()).isTrue();
    assertThat(processedRow.getHasBlankContent()).isFalse();
    assertThat(processedRow.getMessageContent()).isNull();
  }

  @Test
  void shouldProcessedAsCALLByDuration() throws JsonProcessingException {
    ProcessedFile file = new ProcessedFile("testFile", Duration.ZERO);
    String json = "{\"message_type\":\"\",\"timestamp\": null, \"origin\": null,\"destination\": null," +
            "\"duration\": 120,\"status_code\": null,\"status_description\": null}";

    var result = underTest.processJsonString(file, Collections.singletonList(json));

    assertThat(result).hasSize(1);

    final ProcessedRow processedRow = result.get(0);

    assertThat(processedRow.getType()).isEqualTo(MessageType.CALL);
    assertThat(processedRow.getOriginCode()).isEmpty();
    assertThat(processedRow.getDestinationCode()).isEmpty();
    assertThat(processedRow.getDuration()).isEqualTo(120);
    assertThat(processedRow.getStatusCode()).isNull();
    assertThat(processedRow.getHasMissingFields()).isTrue();
    assertThat(processedRow.getHasErrors()).isTrue();
    assertThat(processedRow.getHasBlankContent()).isFalse();
    assertThat(processedRow.getMessageContent()).isNull();
  }

  @Test
  void shouldProcessedAsMSGWhenFieldsEmptyByMsgType() throws JsonProcessingException {
    ProcessedFile file = new ProcessedFile("testFile", Duration.ZERO);
    String json = "{\"message_type\": \"MSG\",\"timestamp\": null,\"origin\": null," +
            "\"destination\": null,\"message_content\": null,\"message_status\": null}";

    var result = underTest.processJsonString(file, Collections.singletonList(json));

    assertThat(result).hasSize(1);

    final ProcessedRow processedRow = result.get(0);

    assertThat(processedRow.getType()).isEqualTo(MessageType.MSG);
    assertThat(processedRow.getOriginCode()).isEmpty();
    assertThat(processedRow.getDestinationCode()).isEmpty();
    assertThat(processedRow.getDuration()).isNull();
    assertThat(processedRow.getStatusCode()).isNull();
    assertThat(processedRow.getHasMissingFields()).isTrue();
    assertThat(processedRow.getHasErrors()).isTrue();
    assertThat(processedRow.getHasBlankContent()).isTrue();
    assertThat(processedRow.getMessageContent()).isNull();
  }

  @Test
  void shouldProcessedAsMSGWhenFieldsEmptyByMsgContent() throws JsonProcessingException {
    ProcessedFile file = new ProcessedFile("testFile", Duration.ZERO);
    String json = "{\"message_type\": \"\",\"timestamp\": null,\"origin\": null," +
            "\"destination\": null,\"message_content\": \"1. HELLO\",\"message_status\": null}";

    var result = underTest.processJsonString(file, Collections.singletonList(json));

    assertThat(result).hasSize(1);

    final ProcessedRow processedRow = result.get(0);

    assertThat(processedRow.getType()).isEqualTo(MessageType.MSG);
    assertThat(processedRow.getOriginCode()).isEmpty();
    assertThat(processedRow.getDestinationCode()).isEmpty();
    assertThat(processedRow.getDuration()).isNull();
    assertThat(processedRow.getStatusCode()).isNull();
    assertThat(processedRow.getHasMissingFields()).isTrue();
    assertThat(processedRow.getHasErrors()).isTrue();
    assertThat(processedRow.getHasBlankContent()).isFalse();
    assertThat(processedRow.getMessageContent()).isEqualTo("1. HELLO");
  }

}