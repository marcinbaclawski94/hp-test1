package com.test1.processing.json.web;

import com.test1.processing.rows.db.ProcessedRowRepository;
import com.test1.processing.rows.domain.ProcessedRow;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;

import javax.annotation.PostConstruct;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;


@TestInstance(TestInstance.Lifecycle.PER_METHOD)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class RecordIT {

  @Autowired
  ProcessedRowRepository rowRepository;
  @LocalServerPort
  private int port;
  private String uri;

  @PostConstruct
  void init() {
    uri = "http://localhost:" + port;
  }

  @Test
  void shouldProcessedFile() {
    given()
            .pathParam("date", "20180201")
            .when()
            .get(uri + "/api/{date}")
            .then()
            .statusCode(HttpStatus.OK.value());

    final List<ProcessedRow> rows = rowRepository.findAll();

    assertThat(rows).hasSizeGreaterThan(1);

  }


  @Test
  void shouldReturnNotFoundFileNotExist() {
    given()
            .pathParam("date", "20200201")
            .when()
            .get(uri + "/api/{date}")
            .then()
            .statusCode(HttpStatus.NOT_FOUND.value());

  }

  @Test
  void shouldReturnErrorWrongJsonFormat() {
    given()
            .pathParam("date", "20180202")
            .when()
            .get(uri + "/api/{date}")
            .then()
            .statusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
  }

  @Test
  void shouldReturnErrorFileAlreadyProcessed() {
    given()
            .pathParam("date", "20180131")
            .when()
            .get(uri + "/api/{date}")
            .then()
            .statusCode(HttpStatus.OK.value());

    final List<ProcessedRow> rows = rowRepository.findAll();

    assertThat(rows).hasSizeGreaterThan(1);

    given()
            .pathParam("date", "20180131")
            .when()
            .get(uri + "/api/{date}")
            .then()
            .statusCode(HttpStatus.CONFLICT.value());


  }

}
