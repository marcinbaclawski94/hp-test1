package com.test1.kpi.web;

import com.test1.kpi.domain.KpisView;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;

import javax.annotation.PostConstruct;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@AutoConfigureTestDatabase
class KpisControllerTestIT {

    @LocalServerPort
    private int port;
    private String uri;

    @PostConstruct
    void init() {
        uri = "http://localhost:" + port;
    }

    @Test
    void shouldReturnNoContent() {
        given()
                .when()
                .get(uri + "/kpis")
                .then()
                .statusCode(HttpStatus.NO_CONTENT.value());
    }

        @Test
    void shouldReturnKpis() {
        given()
                .pathParam("date", "20200101")
                .when()
                .get(uri + "/api/{date}")
                .then()
                .statusCode(HttpStatus.OK.value());


        final KpisView result = given()
                .when()
                .get(uri + "/kpis")
                .then()
                .statusCode(HttpStatus.OK.value())
                .extract().as(KpisView.class);

        assertThat(result.getTotalNumberOfProcessedJsonFiles()).isEqualTo(1);
        assertThat(result.getTotalNumberOfCalls()).isEqualTo(3);
        assertThat(result.getTotalNumberOfMessages()).isEqualTo(3);
        assertThat(result.getTotalNumberOfRows()).isEqualTo(6);
        assertThat(result.getTotalNumberOfDifferentOriginCountryCodes()).isEqualTo(1);
        assertThat(result.getTotalNumberOfDifferentDestinationCountryCodes()).isEqualTo(1);
        assertThat(result.getFileProcessingTimes()).isNotEmpty();
    }
}